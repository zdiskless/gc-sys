#include <stdlib.h>
#include <stdio.h>
#include <winsock2.h>
#include <windows.h>

#include "accept_thread.h"
#include "worker_thread.h"
#include "zmisc_socket.h"

#include "socket_msg.h"


#pragma comment( lib, "Ws2_32.lib")




int Test_Client()
{
    SOCKET clientSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);    //创建流式套接字
    SOCKADDR_IN clientsock_in;                                          //专门针对Internet 通信域的Winsock地址结构
    clientsock_in.sin_addr.S_un.S_addr = 0x0100007f;        //通过inet_addr结构指定套接字的主机IP地址 
    clientsock_in.sin_family = AF_INET;                                 //指定协议家族:AF_INET
    clientsock_in.sin_port = htons(55642);                               //指定将要分配给套接字的传输层端口号：6000

                                                                        //开始连接
    int fail = connect(clientSocket, (SOCKADDR*)&clientsock_in, sizeof(SOCKADDR));
    if (fail){
        return 0;
    }

    struct socket_msg_request st_msg;
    struct socket_msg_response st_msg_rsp;

    memset( &st_msg, 0, sizeof(st_msg) );
    st_msg.ui_msg_len  = sizeof( st_msg );
    st_msg.ui_msg_type = 1;

    send(clientSocket, (const char *)&st_msg, sizeof(st_msg), 0);
    recv(clientSocket, (char *)&st_msg_rsp, sizeof(st_msg_rsp), 0);

    closesocket(clientSocket);//关闭套接字

    return 0;
}




void *g_p_accept = nullptr;
void *g_p_work  = nullptr;


int pfn_on_accpet( void* p_user_data, SOCKET s )
{
    iocp_attach( g_p_work, s);
    return 0;
}


int pfn_on_recv_msg( void *p_worker, char *pc_msg_data )
{
    struct socket_msg_request  *pst_msg     = (struct socket_msg_request *)pc_msg_data;
    struct socket_msg_response *pst_msg_rsp = (struct socket_msg_response *)pc_msg_data;

    pst_msg_rsp->ui_msg_len  = pst_msg->ui_msg_len + sizeof( unsigned int );
    pst_msg_rsp->ui_msg_code = 0;

    printf( "msg:%u\n", pst_msg->ui_msg_type );

    //return 0: back msg
    //rerurn othre, close
    
    return 0;
}


int main()
{
    zmisc_socket_wsa_startup();


    g_p_accept = start_accept_thread( nullptr, 0, 55642, pfn_on_accpet );
    g_p_work   = start_work_thread( pfn_on_recv_msg );

    Test_Client();
    getchar();


    zmisc_socket_wsa_cleanup();
}
