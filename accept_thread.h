#ifndef __accept_thread_h__
#define __accept_thread_h__
    int stop_accept_thread( void * p_accept_handle );
    void * start_accept_thread( void* p_user_data, unsigned long ul_ipv4, unsigned short us_listen_port, int (*pfn_on_accpet)( void* p_user_data, SOCKET s ) );
#endif