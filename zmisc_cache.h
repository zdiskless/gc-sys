#ifndef __zmisc_cache_h__
#define __zmisc_cache_h__

void *zmisc_init_cache(size_t s_max_cache_count, size_t s_entry_size );
int  zmisc_free_cache( void *p_cache);
void *zmisc_alloc_cache_data( void *p_cache );
int  zmisc_free_cache_data( void *p_cache, void *p_cache_data );

#endif //__zmisc_cache_h__
