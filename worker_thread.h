#ifndef __worker_thread_h__
#define __worker_thread_h__

    int iocp_attach( void* p_worker, SOCKET s_socket );
    int stop_work_thread( void* p_worker );
    void *start_work_thread( int (*pfn_on_recv_msg)( void *p_worker, char *pc_msg_data ) );

#endif