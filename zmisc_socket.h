#ifndef __zmisc_socket_h__
#define __zmisc_socket_h__
    int  zmisc_socket_wsa_startup( void );
    int  zmisc_socket_wsa_cleanup( void );
    int  zmisc_socket_listen_tcp_ipv4( unsigned short int usi_port );
    int  zmisc_socket_listen_tcp_ipv6( unsigned short int usi_port );
    int  zmisc_socket_close( int i_socket );
    void zmisc_socket_set_keepalive(int i_socket, int i_timeout);
    void zmisc_socket_set_default_option( int i_socket );
    int  zmisc_socket_set_reuse_addr( int i_socket );
    int  zmisc_socket_get_accept_ex_func( int i_socket, void** ppfn_accept_ex );
    int  zmisc_sock_set_keepalive( int i_socket, int i_timeout );
    int  zmisc_sock_block_recv( int i_socket, char *p_bufer, int i_len );
    int  zmisc_sock_block_send( int i_socket, char *p_bufer, int i_len );
#endif //__zmisc_socket_h__
