#ifndef __socket_msg_h__
#define __socket_msg_h__

#define MAX_SOCKET_MSG_LEN 4096

struct socket_msg_request
{
    unsigned int ui_msg_len;
    unsigned int ui_msg_type;
    //char         c_buffer[1];
};

struct socket_msg_response
{
    unsigned int ui_msg_len;
    unsigned int ui_msg_type;
    unsigned int ui_msg_code;
    //char         c_buffer[1];
};


/*
struct socket_msg_sub_user : public socket_msg_request
{
    char         c_buffer[512];
};
*/


#endif